import api from "./Api";
import { useState, useEffect } from "react";


const GetProducts = () => {
    const [products, setProducts] = useState([]);
 
    useEffect(() => {
        const fetchData = async () => {
            const response = await api.get('products');
            const responseArray = response.data;
            setProducts(responseArray);
        }
        fetchData();
    }, [])

    return (products)
}

export default GetProducts