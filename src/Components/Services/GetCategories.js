import api from "./Api";
import { useState, useEffect } from "react";


const GetCategories = () => {
    const [categories, setCategory] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const response = await api.get('categories');
            const responseArray = response.data;
            setCategory(responseArray);
        }
        fetchData();
    }, [])

    return (categories)
}

export default GetCategories
