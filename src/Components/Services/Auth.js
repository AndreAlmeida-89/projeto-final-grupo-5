import api from "./Api";


const validateUser = (setIsAdmin, setIsLogged) =>{
    const validateConfig = {
        headers:{
          "Content-Type": "application/json",
          "Authorization": localStorage.getItem('token')
        }
    }
    console.log(validateUser)
    api.get('current_user', validateConfig)
    .then(resp => {
        console.log(resp);
        setIsLogged(true);
        if(resp.data.client.kind === "admin"){
            setIsAdmin(true)
       }
    })
    .catch(error => {
        console.log(error);
    })
    
};

export default validateUser