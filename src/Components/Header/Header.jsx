import React from 'react'
import './Header.css'
import { Link } from "react-router-dom";
import logo from '../../Pages/Home/img/logo-branca.png'
import logout from '../Services/Logout';
  

const HeaderAdmin = () =>{
    return(
        <header>
        <Link to="/"><img src={logo} alt="Logo in junior" height="50vh"/></Link>
                
                <h1>Header Admin</h1>
                <h1>Carrinho</h1>
                <ul>
                    <li>
                        <Link to="/loja"> Produtos </Link>
                    </li>

                    <li>
                        <Link to="/dashboard"> Administração </Link>
                    </li>

                    <li>
                        <Link to="/editarperfil"> Editar Perfil </Link>
                    </li> 

                    <li>
                        <Link onClick={logout}> Sair </Link>
                    </li>  
                    
                      
                    
                </ul>
        </header>
    )
}

const HeaderNotLogged = () =>{
    return(
        <header>
         <Link to="/"><img src={logo} alt="Logo in junior" height="50vh"/></Link>
            <h1>Header visitante</h1>
            
            <ul>
                <li>
                    <Link to="/loja"> Produtos </Link>
                </li>

                <li>
                    <Link to="/login"> Login </Link>
                </li>

                <li>
                    <Link to="/cadastro"> Cadastro </Link>
                </li>


            </ul>
        </header>
    )
}

const HeaderClient = () =>{
    return(
        <header>
         <Link to="/"><img src={logo} alt="Logo in junior" height="50vh"/></Link>
            <h1>Header cliente</h1>
            <h1>Carrinho</h1>
            <ul>
                <li>
                    <Link to="/loja"> Produtos </Link>
                </li>

                <li>
                    <Link to="/dashboard"> Administração </Link>
                </li>

                <li>
                    <Link to="/editarperfil"> Editar Perfil </Link>
                </li> 

                  <li>
                    <Link onClick={logout}> Sair </Link>
                </li>    
                
            </ul>
        </header>
    )
}




export {HeaderAdmin, HeaderNotLogged, HeaderClient}

