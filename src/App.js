import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import "./App.css";
import Home from "./Pages/Home/Home";
import Login from "./Pages/Home/Login";
import Loja from "./Pages/Produtos/Loja";
import Cadastro from "./Pages/Home/Cadastro";
import Footer from "./Components/Footer/Footer";
import DashBoard from "./Pages/Dashboard/DashBoard";
import EditarPerfil from "./Pages/Home/EditarPerfil";
import {
  HeaderAdmin,
  HeaderNotLogged,
  HeaderClient,
} from "./Components/Header/Header";
import validateUser from "./Components/Services/Auth";
import logout from "./Components/Services/Logout";

function App() {
  const [isLogged, setIslogged] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
    validateUser(setIsAdmin, setIslogged);
  }, []);

  return (
    <Router>
      <div className="App">
        {!isLogged ? (
          <HeaderNotLogged />
        ) : isAdmin ? (
          <HeaderAdmin />
        ) : (
          <HeaderClient />
        )}
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/loja" component={Loja} />
          <Route path="/login">
            {" "}
            <Login
              updateLogged={(value) => setIslogged(value)}
              updateAdmin={(value) => setIsAdmin(value)}
            />{" "}
          </Route>
          <Route path="/cadastro" component={Cadastro} />
          <Route  path="/dashboard">
            <DashBoard permission={isAdmin} />
          </Route>
          <Route path="/editarperfil" component={EditarPerfil} />
        </Switch>
        <Footer />
      </div>
    </Router>
  );
}

export default App;
