import React from "react";
import { useForm } from "react-hook-form";
import { withRouter, Link, useRouteMatch } from "react-router-dom";
import { useStateMachine } from "little-state-machine";
import updateAction from "./updateAction";

function Cadastro1(props) {
    const { register, handleSubmit } = useForm();
    const { state, action } = useStateMachine(updateAction);
    
    const onSubit = data => {
      action(data);
      console.log(data);
      props.history.push("./Cadastro3");
    };

    return (
        <form className="segunda-etapa" onSubmit={handleSubmit(onSubit)}>
                <div>
                    <label>
                        Nome do Endereço
                        <input type="text" placeholder="Ex.: Casa" name="adress_name" ref={register}></input>
                    </label>
                </div>
    
                <div>
                    <label>
                        CEP
                        <input type="text" placeholder="XXX-XXXX" name="zip" ref={register}></input>
                        <button>Buscar</button>
                    </label>
                </div>
    
                <div>
                    <label>
                        Endereço
                        <input type="text" placeholder="Logradouro" name="street" ref={register}></input>
                    </label>
                </div>
    
                <div>
                    <label>
                        Número
                        <input type="text" placeholder="Insira o número" name="number" ref={register}></input>
                    </label>
                </div>
    
                <div>
                    <label>
                        Bairro
                        <input type="text" placeholder="Insira seu bairro" name="neighborhood" ref={register}></input>
                    </label>
                </div>
    
                <div>
                    <label>
                        Cidade
                        <input type="text" placeholder="Insira sua cidade" name="city" ref={register}></input>
                    </label>
                </div>
    
                <div>
                    <label>
                        UF
                        <input type="text" placeholder="Insira seu Estado" name="state" ref={register}></input>
                    </label>
                </div>
    
                <div>
                    <input type="submit" className="btn btn-info"></input>
                </div>
    
            </form>
    )
}

export default withRouter(Cadastro1);
