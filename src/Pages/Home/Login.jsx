import React, {useState} from 'react'
import api from '../../Components/Services/Api'
import { Redirect } from 'react-router';
       
    function initialState(){
        return {email: "" ,password: ""}; 
    }

function Login(props) {
   
    const[values,setValues] = useState(initialState);
    const [logged, setLogged] = useState(false);
    function onChange(event){
        const {value,name} = event.target ;

        setValues({
            ...values,
            [name]:value,
        })
    }

    const Logar = (e) =>{
        e.preventDefault();     
        if(values.email === ""||values.password === ""){
            alert('Preencha todos os campos!')
        }else{
            
            let LoginBody = {
                "client": {
                    "email": values.email,
                    "password": values.password

                }
            }
            console.log(LoginBody)
            api.post('login', LoginBody)
            .then(response => {
                console.log(response);
                const respToken = response.data.token;
                const respId = response.data.client.id;
                localStorage.setItem('token', respToken)
                localStorage.setItem('client_id', respId)
                setLogged(true);
                props.updateLogged(true);
                if (response.data.client.kind === "admin"){
                    props.updateAdmin(true);
                } 
            })
            .catch(error => console.log(error)) 
                  
        }
    }

    
    if(logged){ 
        return <Redirect to="/" />
    }
    
    return (
        <div className="login">
           
           <form>
           <h1>Fazer login</h1>    
           <div>
                <label>
                    E-mail:
                    <input name='email' id="e-mail" type="text" placeholder="Insira seu e-mail" onChange={onChange} value={values.email}></input>
                </label>
            </div>   

            <div>
                <label>
                    Senha
                    <input name='password' id="password" type="password" placeholder="Insira sua senha" onChange={onChange} value={values.password}></input>
                </label>
            </div>
            <input type="submit" onClick={Logar} className="btn btn-info"></input>    
           </form>
           
        </div>
    )
}

export default Login
