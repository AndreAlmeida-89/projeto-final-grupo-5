import React from 'react'
import { useForm } from 'react-hook-form';
import api from '../../Components/Services/Api';

function EditarPerfil() {
    const {register, handleSubmit} = useForm();
    const id = localStorage
   
    const onSubmit = (e) => {
        const userBody ={"client": e}
        console.log(userBody);
        api.put('clients/' + 28, userBody)
        .then(resp => console.log(resp))
        .catch(error => console.log(error))
    }

    return (
        <div className="login edit">
            <h1>Editar Perfil</h1>
            <form onSubmit={handleSubmit(onSubmit)}>
            <div>
                <label>
                    Nome
                    <input type="text" placeholder="Insira seu nome" name="name" ref={register} />
                </label>
            </div>
    
            <div>
                <label>
                        E-mail
                        <input type="e-mail" placeholder="Insira seu e-mail" name="email" ref={register}></input>
                </label>
            </div>   
    
            <div>
                <label>
                    Senha
                    <input type="password" placeholder="Insira sua senha" name="password" ref={register}></input>
                </label>
            </div> 
    
            <div>
                <label>
                    Confirmar Senha
                    <input type="password" placeholder="Repita a senha" name="password_confirmation" ref={register}></input>
                    </label>
            </div>

            <div>
                <label>
                    Telefone Fixo
                    <input type="text" placeholder="Ex.: 21-5555-5555" name="phone" ref={register}></input>
                </label>
            </div>
    
            <div>
                <label>
                    Celular
                    <input type="text" placeholder="Ex.: 21-95555-5555"  name="celphone" ref={register} ></input>
                </label>
            </div>

            <div>
                <input type='submit' className="btn btn-info"></input>
            </div>

            </form>
        </div>
    )
}

export default EditarPerfil
