import React from "react";
import { useForm } from "react-hook-form";
import { withRouter } from "react-router-dom";
import { useStateMachine } from "little-state-machine";
import updateAction from "./updateAction";

function Cadastro1(props) {
    const { register, handleSubmit } = useForm();
    const { state } = useStateMachine(updateAction);
    
    const onSubit = data => {
      console.log(state);
    }
   
      return (
        <form className="terceira-etapa" onSubmit={handleSubmit(onSubit)}>
                    <div>
                        <label>
                            Telefone Fixo
                            <input type="text" placeholder="Ex.: 21-5555-5555" name="phone" ref={register}></input>
                        </label>
                    </div>
            
                    <div>
                        <label>
                            Celular
                            <input type="text" placeholder="Ex.: 21-95555-5555" name="celphone" ref={register} ></input>
                        </label>
                    </div>
            
                    <div>
                        <input type='submit' value="Finalizar cadastro" className="btn btn-info"></input>
                    </div>
            
        </form>
    )
}

export default withRouter(Cadastro1)
