import React from 'react'
import { useForm } from 'react-hook-form';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams,
    useRouteMatch
  } from "react-router-dom";

import { StateMachineProvider, createStore } from "little-state-machine";
import Cadastro1 from './Cadastro1';
import Cadastro2 from './Cadastro2';
import Cadastro3 from './Cadastro3';
import './Home.css'

function Cadastro() {
   
    let { path, url } = useRouteMatch();

    createStore({
        client: {}
      });
   
    return (
        <div className="login cadastro">
            
            <StateMachineProvider>
            <h1>Fazer Cadastro:</h1>
     
                    <Route exact path={path}>
                        <Cadastro1 />
                    </Route>

                    <Route path={`${url}/Cadastro2`}>
                        <Cadastro2 />
                    </Route>

                    <Route path={`${url}/Cadastro3`}>
                        <Cadastro3 />
                    </Route>
            </StateMachineProvider> 
       
            
        </div>
    )
}



export default Cadastro
