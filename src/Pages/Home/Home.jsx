import React from 'react'
import logo from './img/logo-branca.png'
import './Home.css'

function Home() {

    return (
        <div>

            <section className='sec-chamativa'>
                <h1>e-Commerce Shopping</h1>
            </section>

            <section className="sec-mais-vendidos">
                <h1>Seção Produtos mais Vendidos</h1>
                <img src="https://http2.mlstatic.com/D_NQ_NP_875384-MLA32834052238_112019-O.jpg"></img>
            </section>

            <section className="sec-sobre">

                <div id="text-sobre">
                    <h1>Sobre nós</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur feugiat facilisis molestie. Donec fermentum tempus nisi, in pulvinar justo pharetra ac. Phasellus suscipit eleifend libero vitae porta. Sed lobortis nibh id magna viverra imperdiet. Nulla ultrices risus in quam condimentum molestie. Mauris eget tellus sed turpis pretium vestibulum sit amet quis nibh. Integer facilisis imperdiet neque, a auctor turpis aliquet et. In hac habitasse platea dictumst.</p>
                </div>

                <img src={logo} alt="" width="400vh"/>
            </section>

        </div> 
                  
    )
}

export default Home;