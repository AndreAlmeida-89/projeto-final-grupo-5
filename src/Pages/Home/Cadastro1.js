import React from "react";
import { useForm } from "react-hook-form";
import { withRouter} from "react-router-dom";
import { useStateMachine } from "little-state-machine";
import updateAction from "./updateAction";


function Cadastro1(props) {
    const { register, handleSubmit } = useForm();
    const { action, state } = useStateMachine(updateAction);
    const onSubit = data => {
      action(data);
      console.log(data);
      props.history.push("./cadastro/Cadastro2");
    };
    
    return (
        <form className="primeira-etapa" onSubmit={handleSubmit(onSubit)}>
        <div>
            <label>
                Nome
                <input type="text" placeholder="Insira seu nome" name="name" ref={register} />
            </label>
        </div>

        <div>
            <label>
                    E-mail
                    <input type="e-mail" placeholder="Insira seu e-mail" name="e-mail" ref={register}></input>
            </label>
        </div>   

        <div>
            <label>
                Senha
                <input type="password" placeholder="Insira sua senha" name="password" ref={register}></input>
            </label>
        </div> 

        <div>
            <label>
                Confirmar Senha
                <input type="password" placeholder="Repita a senha" name="password_confirmation" ref={register}></input>
                </label>
        </div>

        <div>    
            <input type="submit" className="btn btn-info"></input>
        </div>

</form>
)
}

export default withRouter(Cadastro1);
