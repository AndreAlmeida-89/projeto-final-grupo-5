import React,{useState} from "react";
import teclado from "../../Assets/teclado.jpg"
import GetCategories from "../../Components/Services/GetCategories";
import api from "../../Components/Services/Api";


function ProdutoSingle(props) {
  const [newcomment, setNewComment] = useState(" ");

  const clientid = localStorage.getItem('client_id')
  
  const categories = GetCategories();

  function onChangeComment(e){
    setNewComment(e.target.value)
  }

  const darLikeComentario = (x) =>{
    const likeCommentBody ={
      "like_comment": {
        "comment_id": x,
        "client_id": clientid
      }
    }
    console.log(likeCommentBody)
      api.post('like_comments',likeCommentBody)
      .then((resp) =>console.log(resp))
      .catch((error)=>console.log(error))
  }
  const darLikeProduto = () =>{
    const likeProductBody ={
      "like_product":{
        "client_id": clientid,
        "product_id": props.produto.id
      }
    }
    console.log(likeProductBody)
    api.post('like_products',likeProductBody)
    .then((resp) =>console.log(resp))
    .catch((error)=>console.log(error))
  }

  const Comentar = () =>{
    const comentBody ={
      "comment":{
        "message": newcomment,
        "client_id": clientid,
        "product_id": props.produto.id
      }
    }
    api.post('comments',comentBody)
    .then((resp) =>console.log(resp))
    .catch((error)=>console.log(error))
  }
  return (

    <div className="card mb-3" style={{maxWidth: '700px'}}>
        <div className="row no-gutters">
          <div className="col-md-4">
            <img src={props.produto.image} className="card-img" alt="teclado" />
          </div>
          <div className="col-md-8">
            <div className="card-body">
              <h2 className="card-title">{props.produto.name}</h2>
                <p className="card-text">
                  <small className="text-muted">Categoria: 
                    {categories.filter(x=> x.id == props.produto.category_id).map(x=>x.name)}
                  </small>
                </p>
              <div>
                <form>
                <label >Quantidade
                  <input type="number" name="quantity" min="1" max="5"></input>
                </label>
                <input type="submit" value="Comprar"></input>
                </form>
              </div>
              <p className="card-text">{props.produto.description}</p>
              <h3 className="card-text">R${props.produto.price},00</h3>
            </div>
          </div>
          <h3>Avaliações:</h3>
          <div id="comentar">
              <input type="text" onChange={onChangeComment}/>
              <input type="button" onClick={Comentar} value="Comentar"/>
          </div>
          <div id="comments">
          {props.produto.comments.map(comment => (
                        <div key={comment.id}>
                            <h2>{comment.title}</h2>
                            <p>{comment.message}</p>
                        </div>      
                    ))}       
          </div>
        </div>

    </div>
  );
}

export default ProdutoSingle;
