import React from "react";
import teclado from "../../Assets/teclado.jpg";
import "./Produtos.css";
import { Link, useRouteMatch } from "react-router-dom";

import GetCategories from "../../Components/Services/GetCategories";

function ProdutoCard(props) {

  let { url } = useRouteMatch();
  
  const categories = GetCategories();
  return (
    <div className="card border-dark mb-3">
      <img className="card-img-top" src={props.produto.image} alt="teclado" />
      <div className="card-body">
        <h5 className="card-title">
          {props.produto.name}
        </h5>
        <p className="card-text">
          2314 compras
        </p>
        <p className="card-text">
          {categories.filter(x=> x.id == props.produto.category_id).map(x=>x.name)}
        </p>
        <p className="card-text">
          {props.produto.description}
        </p>
        <h3 className="card-text">
          R$ ${props.produto.price},00
        </h3>
        <Link key={props.produto.id} to={`${url}/${props.produto.id}`}>
          <button>Ver Mais</button>
        </Link>
      </div>
    </div>
  );
}

export default ProdutoCard;
