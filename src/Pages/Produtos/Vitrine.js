import React from "react";
import ProdutoCard from "./ProdutoCard";
import GetProducts from "../../Components/Services/GetProducts";

function Vitrine() {
  const products = GetProducts();

  return (
    <div className="card-group">
      {products.map((product) => (<ProdutoCard key={product.id} produto={product}/>))}
    </div>
  );
}

export default Vitrine;
