import React from 'react'
import GetCategories from '../../Components/Services/GetCategories'

function Pesquisa() {
    
    const categories = GetCategories();

    return (
        <div id="search">
            <form>
                <div>
                    <label>
                        Digite um produto
                        <input type="text"  name="produto"/>
                    </label>
                </div>

                <div>
                    <label>
                        Categoria
                        <select>
                        {categories.map(category => (
                        <option key={category.id} value={category.id}> {`${category.name}`} </option>      
                         ))
                        }
                        </select>
                    </label>
                </div>

            </form>
        </div>
    )
}

export default Pesquisa
