import React from "react";
import ProdutoCard from "./ProdutoCard";
import Vitrine from "./Vitrine";
import Pesquisa from "./Pesquisa";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useRouteMatch,
} from "react-router-dom";
import GetProducts from "../../Components/Services/GetProducts";
import ProdutoSingle from "./ProdutoSingle";

function Loja() {
  let { path, url } = useRouteMatch();
  const products = GetProducts();
  return (
    <Router>
      <Switch>
        <Route exact path={path} >
          <div id="top">
          <h1>Loja</h1>
          <Pesquisa />
          </div>
          <Vitrine />
        </Route>
        <Route>
          {products.map((product) => (
            <Route key={product.id} path={`${url}/${product.id}`}>
              <ProdutoSingle key={product.id} produto={product} />
            </Route>
          ))}
        </Route>
      </Switch>
    </Router>
  );
}

export default Loja;
