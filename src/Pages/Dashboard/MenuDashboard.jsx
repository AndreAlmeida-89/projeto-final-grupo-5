import React from 'react'
import { Link } from "react-router-dom";

function MenuDashboard() { 
    return (
        <div id="menu">
            <nav>
                <ul className="nav">
                    <li>
                        <Link to="/dashboard/adminprodutos"> Produtos e Categorias </Link>
                    </li>

                    <li>
                        <Link to="/dashboard/adminusuarios"> Usuários </Link>
                    </li>

                    <li>
                        <Link to="/dashboard/adminestoque"> Estoque </Link>
                    </li>

    
                </ul>
            </nav>
        </div>
    )
}

export default MenuDashboard
