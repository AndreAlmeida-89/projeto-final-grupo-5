import React, { useState, useEffect } from 'react'
import api from '../../Components/Services/Api';
import GetCategories from '../../Components/Services/GetCategories';

function GerenciarCategorias() {
    const [newcategory, setNewCategory] = useState(" ");
    const category = GetCategories();
    
    const deleteCategory = (e, id, name) => {
        e.preventDefault();
            if(window.confirm(`Tem certeza que deseja excluir a categoria ${name}?`)){
                api.delete('categories/' + id)
                .then(resp => {
                    if(resp.data != null){
                        alert(`A categoria ${name} foi removida com secesso.`);    
                    }
                });
            }
        e.target.parentNode.parentNode.remove(); 
        }
    const editCategory = () => {

    }
    function onChange(e){
        setNewCategory(e.target.value)
    }
    const createCategory = () =>{
        const updateConfig = {
            "category":{
                "name": newcategory
            }
        }
        
        api.post('categories', updateConfig)
        .then((resp) =>console.log(resp))
        .catch((error)=>console.log(error))
    }
    
    return (
        <div id="products">
            <div id="table">
            <table className="table">
                <thead>
                    <tr>
                        <th>Nome do Produto</th>
                        <th>Deletar</th> 
                    </tr>
                </thead>
                <tbody>
                    {category.map(category => (
                        <tr key={category.id}>
                            <td>{category.name}</td>
                            <td><button onClick={(e) => deleteCategory(e, category.id, category.name)}>Remover</button></td>
                        </tr>      
                    ))}
                    <tr>
                        <td>
                            <input type="text" onChange={onChange} ></input>
                        </td>
                        <td>
                            <input type="submit" value="Adicionar Categoria" onClick={createCategory}></input>
                        </td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
           </div> 
        </div>
    )
}

export default GerenciarCategorias
