import React, { useEffect, useState } from 'react';
import api from '../../Components/Services/Api'

function Estoque() {
    const [products, setProducts] = useState([]);
    const [upquantity, setUpQuantity] = useState(0);

    useEffect(() => {
        const fetchData = async () => {
            const response = await api.get('products');
            const responseArray = response.data;
            setProducts(responseArray);
            
        }
        fetchData();
    }, [])

    const Update = (e, id , name, quantity) => {
        e.preventDefault();
        

        const updateConfig = {
            "product":{
                "quantity": upquantity
            }
        }
        console.log(updateConfig)
        if(window.confirm(`Tem certeza que quer mudar o estoque de ${name} , de ${quantity} para ${upquantity}?`)){
            api.put('products/' + id , updateConfig)
            .then(resp => {
                console.log(resp)
                if(resp.data != null){
                alert(`O produto ${name} foi removido com secesso.`);    
            }
            })
            .catch(error => console.log(error))
            
        }
    }

    function onChange(event){
        setUpQuantity(event.target.value)
    }

    return (
        <div id="estoque">
           <h1>Estoque</h1>
            
           <table className="table">
                <thead>
                    <tr>
                        <th>Nome:</th>
                        <th>Estoque:</th>
                        <th>Preço:</th> 
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {products.map(product => (
                        <tr key={product.id}>
                            <td>{product.name}</td>
                            <td>{product.quantity}</td>
                            <td>{product.price}</td>
                            <td><input type="float" name="quantity" onChange={onChange} placeholder="insira o valor atual"/></td>
                            <td><button onClick={(e) => Update(e,product.id, product.name, product.quantity)}>Atualizar estoque</button></td>
                        </tr>      
                    ))}
                </tbody>
            </table>

        </div>
    )
}

export default Estoque
