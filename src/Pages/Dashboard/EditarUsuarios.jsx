import React, { useEffect, useState } from "react";
import api from "../../Components/Services/Api";

function EditarUsuarios() {
  const [users, setUsers] = useState([]);
  const validateConfig = {
    headers: {
      "Content-Type": "application/json",
      Authorization: localStorage.getItem("token"),
    },
  };
  useEffect(() => {
    const fetchData = async () => {
      const response = await api.get("clients", validateConfig);
      const responseArray = response.data;
      setUsers(responseArray);
    };
    fetchData();
  }, [validateConfig]);

  const deleteUser = (e, id, name) => {
    e.preventDefault();
    e.target.parentNode.parentNode.remove();
    if (window.confirm(`Tem certeza que deseja excluir ${name}?`)) {
      api.delete("clients/" + id).then((resp) => {
        if (resp.data != null) {
          alert(`O cliente ${name} foi removido com secesso.`);
        }
      });
    }
  };

  const givAdmin = (id, name) => {
    const adminConfig = {
      client: {
        kind: "admin",
      },
    };

    if (
      window.confirm(`Tem certeza que deseja tornar ${name}? Administrador?`)
    ) {
      api.put("clients/" + id, adminConfig).then((resp) => {
        if (resp.data != null) {
          alert(`${name} foi promovido a administrador da página.`);
        }
      });
    }
  };

  return (
    <div id="users">
      <h1>Usuários</h1>
      <div id="table">
        <table className="table">
          <thead>
            <tr>
              <th>Nome:</th>
              <th>E-mail:</th>
              <th>Permissão:</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {users.map((user) => (
              <tr key={user.id}>
                <td>{user.name}</td>
                <td>{user.email}</td>
                <td>{user.kind}</td>
                <td>
                  <button onClick={() => givAdmin(user.id, user.name)}>
                    Tornar admin
                  </button>
                </td>
                <td>
                  <button onClick={(e) => deleteUser(e, user.id, user.name)}>
                    Remover
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default EditarUsuarios;
