import React, { useState } from "react";
import { useForm, Select } from "react-hook-form";
import api from "../../Components/Services/Api";
import GetCategories from "../../Components/Services/GetCategories";

function CriarProduto() {
  const { register, handleSubmit } = useForm();
  const categories = GetCategories();


  const onSubit = (data) => {
    const productBody = { product: data };
    console.log(productBody);
    api
      .post("products", productBody)
      .then((resp) => console.log(resp))
      .catch((error) => console.log(error));
  };

  return (
    <form onSubmit={handleSubmit(onSubit)}>
      <h2>Criar um produto</h2>
      <div>
        <label>
          Nome
          <input
            type="text"
            placeholder="Insira seu nome"
            name="name"
            ref={register}
          />
        </label>
      </div>

      <div>
          <label>
              Categoria
              <select name="category_id" ref={register} >
              {categories.map(category => (
              <option  key={category.id} value={category.id}> {`${category.name}`} </option>      
                ))
              }
              </select>
          </label>
      </div>

      <div>
        <label>
          Descrição
          <textarea
            type="text"
            placeholder="Escreva a descrição do produto"
            name="description"
            ref={register}
          />
        </label>
      </div>

      <div>
        <label>
          Preço R$
          <input
            type="text"
            placeholder="EX: 9.99"
            name="price"
            ref={register}
          />
        </label>
      </div>

      <div>
        <label>
          Peso
          <input
            type="text"
            placeholder="Ex: 9.999"
            name="weight"
            ref={register}
          />
        </label>
      </div>

      <div>
        <label>
          Dimensões
          <input
            type="text"
            placeholder="Ex: 9.999"
            name="dimension"
            ref={register}
          />
        </label>
      </div>

      <div>
        <label>
          Quantidade
          <input
            type="number"
            placeholder="Ex: 100"
            name="quantity"
            ref={register}
          />
        </label>
      </div>

      <div>
        <label>
          Imagem
          <input
            type="text"
            ref={register}
            name="imageurl"
            placeholder="Insira a URL da imagem"
          ></input>
        </label>
      </div>

      <div>
        <input type="submit" value="Enviar" />
      </div>
    </form>
  );
}

export default CriarProduto;
