import React from "react";

import EditarUsuarios from "./EditarUsuarios";
import Estoque from "./Estoque";
import MenuDashboard from "./MenuDashboard";
import EditarProdutos from "./EditarProdutos";

import { BrowserRouter as Router, Switch, Route, useRouteMatch } from "react-router-dom";

import "./Dashboard.css";
import Login from "../Home/Login";

function DashBoard(props) {
  
  let { path, url } = useRouteMatch();
  
  if(props.permission){
  
    return(
    <Router>
      <div className="dashboard">
        <MenuDashboard />
        <Switch>
          <Route path={path} exact component={EditarProdutos} />
          <Route path={`${url}/adminusuarios`} component={EditarUsuarios} />
          <Route path={`${url}/adminestoque`} component={Estoque} />
          <Route path={`${url}/adminprodutos`} component={EditarProdutos} />
        </Switch>
      </div>
    </Router>
  )
 }
  else{
    return(
      <div>
        <h1>
          Você não é admin!
        </h1>
        <p>
        Faça login como administrador para acessar essa área!
        </p>
        <Login />
      </div>
    )

}
}

export default DashBoard;
