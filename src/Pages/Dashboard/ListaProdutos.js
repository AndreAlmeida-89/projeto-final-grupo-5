import React, { useState, useEffect } from 'react'
import api from '../../Components/Services/Api';

function ListaProdutos() {
    const [products, setProduct] = useState([]);
    // const validateConfig = {
    //     headers:{
    //       "Content-Type": "application/json",
    //       "Authorization": localStorage.getItem('token')
    //     }
    // }
    useEffect(() => {
        const fetchData = async () => {
            const response = await api.get('products');
            const responseArray = response.data;
            setProduct(responseArray);
            console.log(responseArray);
        }
        fetchData();
    }, [])
    
    const deleteProd = (e, id, name) => {
        e.preventDefault();
            if(window.confirm(`Tem certeza que deseja excluir o produto ${name}?`)){
                api.delete('products/' + id)
                .then(resp => {
                    if(resp.data != null){
                        alert(`O produto ${name} foi removido com secesso.`);    
                    }
                });
            }
        e.target.parentNode.parentNode.remove(); 
        }
    
    return (
        <div id="products">
            <div id="table">
            <table className="table">
                <thead>
                    <tr>
                        <th>Nome do Produto</th>
                        <th>Deletar</th> 
                    </tr>
                </thead>
                <tbody>
                    {products.map(product => (
                        <tr key={product.id}>
                            <td>{product.name}</td>
                            <td><button onClick={(e) => deleteProd(e, product.id, product.name)}>Remover</button></td>
                        </tr>      
                    ))}
                </tbody>
            </table>
           </div> 
        </div>
    )
}

export default ListaProdutos
