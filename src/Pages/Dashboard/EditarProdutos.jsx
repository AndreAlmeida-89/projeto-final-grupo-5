import React from "react";
import CriarProduto from "./CriarProduto";
import ListaProdutos from "./ListaProdutos";
import GerenciarCategorias from "./GerenciarCategorias";

function EditarProdutos() {
  return (
    <div id="edit"> 
      <div id="left">
        <div id="gp">
          <h2>Genrenciar Produtos</h2>
          <CriarProduto />
        </div>

        <div id="gc">
          <h2>Gerenciar Categorias </h2>
          <GerenciarCategorias />
        </div> 
      </div>
      
        <div id="lp">
          <h2>Lista de Produtos</h2>
          <ListaProdutos />
        </div>
         
    </div>
  );
}

export default EditarProdutos;
